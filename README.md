# Bayesian_simulation



In the R file provided there are a few codes about :



- Problem of the bayesian simulation (rnorm, maximum of likelihood, runif, rep)



- Example of logistic regression (rt, runif, rep,rbinom, glm, dbinom, rep, kde2d, plot)



- Approximation with a normal law (solve, cov, plot)



- Approximation of the conditional mean by importance sampling (rmt, exp, dmt, plot)



- Calculation of the efficient size (cbind, plot, hist)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

October 2021
